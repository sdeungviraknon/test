import { Body, Controller, Get, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { loginDto } from './DTO/login.dto';
import { signUpDto } from './DTO/signUp.dto';

@Controller('auth')
export class AuthController {

  constructor(private authService:AuthService){}

  @Post('/signUp')
  signUp(@Body() signUP: signUpDto): Promise<{token:string}>{
    return this.authService.signUp(signUP);
  }

  @Get('/login')
  login(@Body() login: loginDto): Promise<{token:string}>{
    return this.authService.login(login);
  }
}
