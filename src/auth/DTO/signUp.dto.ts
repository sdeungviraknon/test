import { IsEmail, IsNotEmpty, IsString, MinLength } from "class-validator";

export class signUpDto{

  @IsNotEmpty()
  @IsString()
  readonly name: string;

  @IsNotEmpty()
  @IsEmail({}, {message: 'Please enter email correctly!'})
  readonly email: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  readonly password: string;
}