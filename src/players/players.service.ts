import { Injectable } from '@nestjs/common';

@Injectable()
export class PlayersService {
  private players = [
    {
      "id": 1,
      "name": "John Smith",
      "position": "Forward",
      "age": 25,
      "nationality": "England",
      "club": "Strikers United"
    },
    {
      "id": 2,
      "name": "Michael Johnson",
      "position": "Midfielder",
      "age": 28,
      "nationality": "Brazil",
      "club": "Greenfield FC"
    },
    {
      "id": 3,
      "name": "David Garcia",
      "position": "Defender",
      "age": 24,
      "nationality": "Spain",
      "club": "City Rangers"
    },
    {
      "id": 4,
      "name": "Emma White",
      "position": "Goalkeeper",
      "age": 26,
      "nationality": "USA",
      "club": "Thunder Strikers"
    },
    {
      "id": 5,
      "name": "Mohammed Ali",
      "position": "Forward",
      "age": 27,
      "nationality": "Egypt",
      "club": "Pyramids FC"
    },
    {
      "id": 6,
      "name": "Luis Hernandez",
      "position": "Midfielder",
      "age": 29,
      "nationality": "Mexico",
      "club": "Aztecs United"
    },
    {
      "id": 7,
      "name": "Sophie Müller",
      "position": "Defender",
      "age": 23,
      "nationality": "Germany",
      "club": "Bavarian Giants"
    },
    {
      "id": 8,
      "name": "Ricardo Silva",
      "position": "Goalkeeper",
      "age": 31,
      "nationality": "Portugal",
      "club": "Maritime FC"
    },
    {
      "id": 9,
      "name": "Juan Martinez",
      "position": "Forward",
      "age": 26,
      "nationality": "Argentina",
      "club": "Buenos Aires FC"
    },
    {
      "id": 10,
      "name": "Chen Wei",
      "position": "Midfielder",
      "age": 24,
      "nationality": "China",
      "club": "Dragon City FC"
    },
    {
      "id": 11,
      "name": "Alessandro Rossi",
      "position": "Defender",
      "age": 27,
      "nationality": "Italy",
      "club": "Roman Gladiators"
    },
    {
      "id": 12,
      "name": "Elena Petrova",
      "position": "Goalkeeper",
      "age": 25,
      "nationality": "Russia",
      "club": "Moscow Bears"
    },
    {
      "id": 13,
      "name": "Lucas Fernandes",
      "position": "Forward",
      "age": 28,
      "nationality": "Brazil",
      "club": "Samba FC"
    },
    {
      "id": 14,
      "name": "Hassan Al-Mansoori",
      "position": "Midfielder",
      "age": 30,
      "nationality": "Qatar",
      "club": "Doha Stars"
    },
    {
      "id": 15,
      "name": "Yuki Tanaka",
      "position": "Defender",
      "age": 26,
      "nationality": "Japan",
      "club": "Tokyo Samurai"
    },
    {
      "id": 16,
      "name": "María García",
      "position": "Goalkeeper",
      "age": 27,
      "nationality": "Spain",
      "club": "Barcelona Lions"
    },
    {
      "id": 17,
      "name": "Andrei Ivanov",
      "position": "Forward",
      "age": 25,
      "nationality": "Russia",
      "club": "St. Petersburg Dynamo"
    },
    {
      "id": 18,
      "name": "Sarah Johnson",
      "position": "Midfielder",
      "age": 23,
      "nationality": "England",
      "club": "London Royals"
    },
    {
      "id": 19,
      "name": "Pedro Martinez",
      "position": "Defender",
      "age": 29,
      "nationality": "Spain",
      "club": "Real Madrid"
    },
    {
      "id": 20,
      "name": "Kim Lee",
      "position": "Goalkeeper",
      "age": 26,
      "nationality": "South Korea",
      "club": "Seoul Tigers"
    }
  ]
  ;

  getAllPlayers(){
    return this.players;
  }
}
