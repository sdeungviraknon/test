import { IsEmpty, IsEnum, IsNotEmpty, IsNumber, IsString } from "class-validator";
import { User } from "src/auth/schemas/user.schema";
import { Category } from "../schemas/book.schema";

export class CreateBookDto{

  @IsNotEmpty()
  @IsString()
  readonly title: string;

  @IsNotEmpty()
  @IsString()
  readonly description: string;

  @IsNotEmpty()
  @IsString()
  readonly author: string;

  @IsNotEmpty()
  @IsNumber()
  readonly price: number;

  @IsNotEmpty()
  @IsEnum(Category,{message:"Invalid category"})
  readonly category:Category;

  @IsEmpty({ message: 'User id is required if you are not logged in' })
  readonly user: User;
}