import { IsEmpty, IsEnum, IsNumber, IsOptional, IsString } from "class-validator";
import { User } from "src/auth/schemas/user.schema";
import { Category } from "../schemas/book.schema";

export class updateBookDto{

  @IsOptional()
  @IsString()
  readonly title: string;

  @IsOptional()
  @IsString()
  readonly description: string;

  @IsOptional()
  @IsString()
  readonly author: string;

  @IsOptional()
  @IsNumber()
  readonly price: number;

  @IsOptional()
  @IsEnum(Category, {message : 'Invalid category'})
  readonly category:Category;

  @IsEmpty({ message: 'User id is required if you are not logged in' })
  readonly user: User;
}