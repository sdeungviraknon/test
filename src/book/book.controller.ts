import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Req, UseGuards } from '@nestjs/common';
import { BookService } from './book.service';
import { CreateBookDto } from './DTO/createBook.dto';
import { updateBookDto } from './DTO/updateBook.dto';
import { Book } from './schemas/book.schema';
import { Query as ExpressQuery  } from 'express-serve-static-core';
import { AuthGuard } from '@nestjs/passport';

@Controller('books')
export class BookController {

  constructor( private booksService: BookService){}

  @Get()
  async getAllBooks(@Query() query:ExpressQuery): Promise<Book[]> { //@Query() query:ExpressQuery for search query
    return this.booksService.findAll(query);
  }
  
  @Get(':id')
  async getBookById(@Param('id') id:string): Promise<Book> {
    return this.booksService.findById(id);
  }

  @Post()
  @UseGuards(AuthGuard()) // TODO : Implement JWT Auth
  async createNewBook(@Body() book:CreateBookDto, @Req() req): Promise<Book>{
    return this.booksService.create(book, req.user);
  }

  @Patch(':id')
  async updateBookById(
      @Param('id') 
      id:string,
      @Body()
      book:updateBookDto
    ): Promise<Book>{
    return this.booksService.updateById(id, book);
  }

  @Delete(':id')
  async deleteBookById(@Param('id') id:string): Promise<Book> {
    return this.booksService.deleteById(id);
  }
  
}
